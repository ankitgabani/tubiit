//
//  StoreitemCollectionViewCell.swift
//  Tubiit
//
//  Created by SMIT KUKADIYA on 06/08/20.
//  Copyright © 2020 ANKIT GABANI. All rights reserved.
//

import UIKit

class StoreitemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var uiview: UIView!
    @IBOutlet weak var lbloffer: UILabel!
    @IBOutlet weak var imgproduct: UIImageView!
    
    
    @IBOutlet weak var lblCompanyname: UILabel!
    
    @IBOutlet weak var lblProductname: UILabel!
    
    
    @IBOutlet weak var lblprice: UILabel!
    
    @IBOutlet weak var lblstrikeprice: UILabel!
    
}
