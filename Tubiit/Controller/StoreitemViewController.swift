//
//  StoreitemViewController.swift
//  Tubiit
//
//  Created by SMIT KUKADIYA on 06/08/20.
//  Copyright © 2020 ANKIT GABANI. All rights reserved.
//

import UIKit

class StoreitemViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
  
   
    @IBOutlet weak var Textcollectionview: UICollectionView!
    @IBOutlet weak var Productcolletionview: UICollectionView!
    
    
    var Arrtextheader = ["Allll","Unari Leg Bags","Bedside drainage Bages","Unari Bags","Allll","Unari Leg Bags"]
    var Arrlbloffer = ["Upto 50% OFF","Upto 30% OFF","Upto 20% OFF","Upto 10% OFF","Upto 90% OFF","Upto 35% OFF"]
    var Arrproductimage = ["image 1","image 1","image 1","image 1","image 1","image 1"]
    
     var ArrCompanyname = ["Bard Inc","Bard Inc","Bard Inc","Bard Inc","Bard Inc","Bard Inc"]
    
      var ArrProductname = ["Bard Bardia Closed\n System Urinary\n Drainage Bag","Bard Bardia Closed\n System Urinary\n Drainage Bag","Bard Bardia Closed\n System Urinary\n Drainage Bag","Bard Bardia Closed\n System Urinary\n Drainage Bag","Bard Bardia Closed\n System Urinary\n Drainage Bag","Bard Bardia Closed\n System Urinary\n Drainage Bag"]
    
     var Arrprice = ["$2.29","$2.29","$2.29","$2.29","$2.29","$2.29"]
    
    var Arrstikeprice = ["$3.00","$3.00","$3.00","$3.00","$3.00","$3.00"]


    override func viewDidLoad() {
        super.viewDidLoad()

        
        Textcollectionview.delegate = self
        Textcollectionview.dataSource = self
        Productcolletionview.delegate = self
        Productcolletionview.dataSource = self
        
        
    }
    
    
    @IBAction func Btnback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == Textcollectionview{
            
            return Arrtextheader.count
        }else{
            return Arrlbloffer.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == Textcollectionview{
            let cell = Textcollectionview.dequeueReusableCell(withReuseIdentifier: "StoreitemeteproductCollectionViewCell", for: indexPath) as!                     StoreitemeteproductCollectionViewCell
            
                cell.Lblheader.text = Arrtextheader[indexPath.row]
                return cell
        }else{
            let cell = Productcolletionview.dequeueReusableCell(withReuseIdentifier: "StoreitemCollectionViewCell", for: indexPath) as! StoreitemCollectionViewCell
                   
                cell.lbloffer.text = Arrlbloffer[indexPath.row]
                cell.lblCompanyname.text = ArrCompanyname[indexPath.row]
                cell.lblProductname.text = ArrProductname[indexPath.row]
                cell.lblprice.text = Arrprice[indexPath.row]
                cell.lblstrikeprice.text = Arrstikeprice[indexPath.row]
                cell.imgproduct.image = UIImage(named: Arrproductimage[indexPath.row])
                
            
           // cell.layer.borderWidth = 1
            
                return cell
        
        }
           
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == Textcollectionview{
            
                   let label = UILabel(frame: CGRect.zero)
                   label.text = Arrtextheader[indexPath.item]
                   label.sizeToFit()
                   return CGSize(width: label.frame.width, height: 34)
        }
        else{
            
//                       let width = (UIScreen.main.bounds.size.width) / 2
//
//                       return CGSize.init(width: width, height: 200)
            
            let width = self.view.frame.size.width/2
            
            return CGSize.init(width: width, height: 310)
            
        }
    }
   

   

}
